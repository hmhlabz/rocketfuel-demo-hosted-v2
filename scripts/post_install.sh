#!/usr/bin/env bash
set -e

cd ~/$APPLICATION_NAME

[ -d "node_modules" ] && echo "Directory node_modules exists so cleaning up the directory"; rm -rf node_modules

npm install typescript
npm install

# add node to startup
hasRc=`grep "su -l $USER" /etc/rc.d/rc.local | cat`
if [ -z "$hasRc" ]; then
    sudo sh -c "echo 'su -l $USER -c \"cd ~/$APPLICATION_NAME/scripts ./run.sh\"' >> /etc/rc.d/rc.local"
fi
