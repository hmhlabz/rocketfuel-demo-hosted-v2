#!/usr/bin/env bash

cd ~/$APPLICATION_NAME

pm2 update
pm2 start app.json
pm2 save

#### CHECK NGINX ####

SERVICE="nginx"
if sudo pgrep -x "$SERVICE" >/dev/null
then
    echo "$SERVICE is running, attempting to reload"
    sudo systemctl reload nginx.service
else
    echo "$SERVICE is stopped, attempting to start it"
     sudo systemctl start nginx.service
fi
