var express = require("express");
var router = express.Router();
var request = require("request");
var crypto=require('crypto');
const Rocketfuel = require('rocketfuel-node-sdk');
// const publicKey1='-----BEGIN PUBLIC KEY-----\n'+
// 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2e4stIYooUrKHVQmwztC\n'+
// '/l0YktX6uz4bE1iDtA2qu4OaXx+IKkwBWa0hO2mzv6dAoawyzxa2jmN01vrpMkMj\n'+
// 'rB+Dxmoq7tRvRTx1hXzZWaKuv37BAYosOIKjom8S8axM1j6zPkX1zpMLE8ys3dUX\n'+
// 'FN5Dl/kBfeCTwGRV4PZjP4a+QwgFRzZVVfnpcRI/O6zhfkdlRah8MrAPWYSoGBpG\n'+
// 'CPiAjUeHO/4JA5zZ6IdfZuy/DKxbcOlt9H+z14iJwB7eVUByoeCE+Bkw+QE4msKs\n'+
// 'aIn4xl9GBoyfDZKajTzL50W/oeoE1UcuvVfaULZ9DWnHOy6idCFH1WbYDxYYIWLi\n'+
// 'AQIDAQAB\n'+
// '-----END PUBLIC KEY-----'
var encryptStringWithRsaPublicKey = function(toEncrypt) {
    var buffer = Buffer.from(toEncrypt);
    var encrypted = crypto.publicEncrypt(process.env.PUBLIC_KEY, buffer);
    return encrypted.toString("base64");
};
console.log();

/* GET home page. */
router.get("/signup",(req,res,next)=>{
  res.sendFile('/home/ttn/Desktop/rocketfuel-demo-hosted-v2/public/signup.html');
})
router.get("/", function (req, res, next) {
  const merchantAuth= encryptStringWithRsaPublicKey(process.env.MERCHANT_ID);
  res.render("checkoutPage", {developmentEnv: process.env.CHECKOUT_ENVIRONMENT,merchantAuth:merchantAuth});
});

//legacy API based approach
router.post("/placeOrder", function (req, res, next) {
  var orderData=req.body;
  orderData["merchant_id"]=process.env.MERCHANT_ID;
  var options = {
    method: "POST",
    url: process.env.API_ENDPOINT + "auth/login",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      email: process.env.MERCHANT_EMAIL,
      password: process.env.MERCHANT_PASS,
    }),
  };
  request(options, function (error, response) {
    if (error) throw new Error(error);
    console.log(JSON.parse(response.body));
    if(JSON.parse(response.body) === undefined || JSON.parse(response.body).result === undefined){
      res.status(400).send({ error: "Failed to place order" });
      res.end();
      return;
    }
    let accessToken = JSON.parse(response.body).result.access;
    //place the order API Call
    var options = {
      method: "POST",
      url: process.env.API_ENDPOINT + "hosted-page",
      headers: {
        authorization: "Bearer " + accessToken,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(orderData),
    };
    request(options, function (error, response) {
      if (error) throw new Error(error);
      let resp = JSON.parse(response.body);
      console.log(resp);
      if(resp.result !== undefined && resp.result.url !== undefined){
        let urlArr = resp.result.url.split("/");
        let uuid = urlArr[urlArr.length - 1];
        res.status(200).send({ uuid: uuid });
      }else{
        res.status(400).send({ error: "Failed to place order" });
      }
    });
  });
});

//new Npm based code
router.get("/placeOrder_node", function (req, res, next) {
  const cartInfo = JSON.stringify({
    amount: "59.97",
    merchant_id: process.env.MERCHANT_ID,
    cart: [
      {
        id: "23",
        name: "Tshirt with round nect (L)",
        price: "10",
        quantity: "1",
      },
      {
        id: "24",
        name: "Flower Formal T-shirt (L)",
        price: "15",
        quantity: "1",
      },
      {
        id: "25",
        name: "Printed White Tshirt (L)",
        price: "9",
        quantity: "1",
      },
    ],
    currency: "USD",
    order: (Date.now()).toString(),
    redirectUrl: "",
  });

  //rocketfuel code starts
  try{
    const rocketfuel = new Rocketfuel(process.env.API_ENDPOINT);
    const authData = {
      email: process.env.MERCHANT_EMAIL,
      password: process.env.MERCHANT_PASS
    }
    rocketfuel.auth(authData)
    .then(auth => {
      rocketfuel.setAccessToken(auth.result.access);
      const resp = rocketfuel.generateUUID(cartInfo).then(resp => {
        if(resp.result !== undefined && resp.result.uuid !== undefined){
          res.status(200).send({ uuid: resp.result.uuid });
        }else{
          res.status(400).send({ error: "Failed to generate UUID" });
        }
      })
    })
  }catch(e){
    console.error(e);
    res.status(400).send({ error: "Failed to generate UUID" });
  }
});

module.exports = router;
